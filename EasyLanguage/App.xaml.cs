﻿using System;
using System.Net.NetworkInformation;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace EasyLanguage
{
    public partial class App : Application
    {
        public static int ScreenHeight { get; set; }
        public static int ScreenWidth { get; set; }

        public static string BaseUrl;
        public static string ContentType;

        public static string SecretKey { get; set; }
        public static string BinId { get; set; }

        public App()
        {
            ContentType = "application/json";
            SecretKey = "$2a$10$AOrDYGd.3u4IjjsiV.FwLuWt3vx2aN0F361sbg0MrxSDl6m3aTO6m";
            BinId = "5c7aa2c10e7529589336d040";
            BaseUrl = "https://api.jsonbin.io/b/" + App.BinId;

            InitializeComponent();

            MainPage = new MainPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
