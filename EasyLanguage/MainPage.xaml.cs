﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyLanguage.View;
using EasyLanguage.Collection;
using EasyLanguage.Model;
using Xamarin.Forms;
using System.Diagnostics;
using System.Net.Http;
using Newtonsoft.Json;

namespace EasyLanguage
{
    public partial class MainPage : ContentPage
    {

        public CardsCollection CardsCollection;
        public string ActiveViewName;
        public Header Header;
        public CardsList CardsList;
        public StartView StartView;
        public CardForm CardForm;

        public MainPage()
        {
            InitializeComponent();

            ActiveViewName = "MainPage";
            RenderCardsCollection();
            RenderHeader();
            RenderCardsList();
            RenderStartView();
            RenderCardForm();
        }

        async private void RenderCardsCollection() 
        {
            CardsCollection = new CardsCollection();

            HttpClient Client = new HttpClient();

            Client.DefaultRequestHeaders.Add("secret-key", App.SecretKey);

            var task = await Client.GetAsync(App.BaseUrl + "/latest");
            var jsonString = await task.Content.ReadAsStringAsync();

            CardsCollection.List = JsonConvert.DeserializeObject<List<CardModel>>(jsonString);
        }

        private void RenderHeader()
        {
            Header = new Header();

            Header.CurrentPage = this;
            Grid.SetRow(Header, 0);

            PageGrid.Children.Add(Header);
        }

        private void RenderCardsList()
        {
            CardsList = new CardsList();

            CardsList.IsVisible = false;
            CardsList.CurrentPage = this;

            AbsoluteLayout.SetLayoutBounds(CardsList, new Rectangle(0, 0, 1, 1));
            AbsoluteLayout.SetLayoutFlags(CardsList, AbsoluteLayoutFlags.All);

            ContentLayout.Children.Add(CardsList);
        }

        private void RenderStartView()
        {
            StartView = new StartView();

            StartView.IsVisible = false;
            StartView.CurrentPage = this;

            AbsoluteLayout.SetLayoutBounds(StartView, new Rectangle(0, 0, 1, 1));
            AbsoluteLayout.SetLayoutFlags(StartView, AbsoluteLayoutFlags.All);

            ContentLayout.Children.Add(StartView);
        }

        private void RenderCardForm() 
        {
            CardForm = new CardForm();

            CardForm.IsVisible = false;
            CardForm.CurrentPage = this;

            AbsoluteLayout.SetLayoutBounds(CardForm, new Rectangle(0,0,1,1));
            AbsoluteLayout.SetLayoutFlags(CardForm, AbsoluteLayoutFlags.All);

            PageLayout.Children.Add(CardForm);
        }

        public void Update()
        {
            CardsList.Render();
            StartView.Render();
        }

        public void CloseActiveView() 
        {
            switch (ActiveViewName)
            {
                case "CardsList":
                    CardsList.Close();
                    break;

                case "StartView":
                    StartView.Close();
                    break;

                default:

                    break;
            }
        }

        private void Handle_Clicked_PageListButton(object sender, System.EventArgs e)
        {
            CardsList.Open();
        }

        private void Handle_Clicked_PageStartButton(object sender, System.EventArgs e)
        {
            StartView.Open();
        }
    }
}
