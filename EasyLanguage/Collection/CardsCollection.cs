﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using EasyLanguage.Model;
using Newtonsoft.Json;

namespace EasyLanguage.Collection
{
    public class CardsCollection
    {
        public List<CardModel> List { get; set; }

        public void AddItem(CardModel Card)
        {
            //add
            if (!List.Exists(x => x.Id == Card.Id))
            {
                List.Insert(0, Card);
            }

            //update
            Sync();
        }

        public void RemoveItem(int Id) 
        {
            var Item = FindItem(Id);

            if (Item != null)
            {
                List.Remove(Item);
                Sync();
            }
        }

        public CardModel FindItem(int Id)
        {
            return List.FirstOrDefault(item => item.Id == Id);
        }

        async public void Sync()
        {
            var json = JsonConvert.SerializeObject(List);

            HttpClient Client = new HttpClient();

            Client.DefaultRequestHeaders.Add("secret-key", App.SecretKey);
            Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(App.ContentType));

            HttpContent Body = new StringContent(json);
            Body.Headers.ContentType = new MediaTypeHeaderValue(App.ContentType);

            HttpResponseMessage response = await Client.PutAsync(App.BaseUrl, Body);
        }
    }

}