﻿using System;
using System.Collections.Generic;
using EasyLanguage.Model;
using Xamarin.Forms;

namespace EasyLanguage.View
{
    public partial class StartView : ContentView
    {

        public MainPage CurrentPage;

        public StartView()
        {
            InitializeComponent();
        }

        public void Render() 
        {
            ContentLayout.Children.Clear();

            foreach (CardModel item in CurrentPage.CardsCollection.List)
            {
                var view = new CardMicro(item);
                view.CurrentPage = CurrentPage;

                ContentLayout.Children.Add(view);
            }
        }

        public void Open()
        {
            this.IsVisible = true;

            CurrentPage.Header.SetTitle("Start View");
            CurrentPage.Header.ShowBackButton();

            CurrentPage.ActiveViewName = "StartView";

            Render();
        }

        public void Close()
        {
            this.IsVisible = false;

            CurrentPage.Header.SetDefaultTitle();
            CurrentPage.Header.HideBackButton();

            CurrentPage.ActiveViewName = "StartView";
        }

    }
}
