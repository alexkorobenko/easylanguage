﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using EasyLanguage.Model;
using System.Diagnostics;

namespace EasyLanguage.View
{
    public partial class CardForm : ContentView
    {

        public MainPage CurrentPage;

        public CardModel Card;

        public CardForm()
        {
            InitializeComponent();
        }

        public void Open()
        {
            this.IsVisible = true;

            Render();
        }

        public void Close()
        {
            this.IsVisible = false;

            Reset();
        }

        public void Render()
        {
            if (Card != null)
            {
                if (Card.OriginTitle != null && Card.OriginTitle.Length > 0)
                {
                    OriginTitle.Text = Card.OriginTitle;
                }

                if (Card.TranslateTitle != null && Card.TranslateTitle.Length > 0)
                {
                    TranslateTitle.Text = Card.TranslateTitle;
                }

                if (Card.Description != null && Card.Description.Length > 0)
                {
                    Description.Text = Card.Description;
                }
            }
        }

        public void Reset()
        {
            Card = null;

            OriginTitle.Text = "";
            TranslateTitle.Text = "";
            Description.Text = "";
        }

        public void Submit() 
        {
            if (Card == null)
            {
                Card = new CardModel();
            }

            if (!string.IsNullOrWhiteSpace(OriginTitle.Text) && !string.IsNullOrWhiteSpace(TranslateTitle.Text))
            {
                //update CardsCollection by link (if card is present in collections)
                Card.OriginTitle = OriginTitle.Text;
                Card.TranslateTitle = TranslateTitle.Text;
                Card.Description = Description.Text;

                //add new item
                CurrentPage.CardsCollection.AddItem(Card);
                CurrentPage.Update();

                Close();
            }
            else
            {
                App.Current.MainPage.DisplayAlert("Error", "Origin title and Translate title is required fields", "Ok");
            }
        }

        void Handle_Clicked_SubmitButton(object sender, System.EventArgs e)
        {
            Submit();
        }

        void Handle_Clicked_CloseButton(object sender, System.EventArgs e)
        {
            Close();
        }
    }

}
