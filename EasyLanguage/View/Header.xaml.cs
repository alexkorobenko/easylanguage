﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace EasyLanguage.View
{
    public partial class Header : ContentView
    {

        public MainPage CurrentPage;

        public Header()
        {
            InitializeComponent();

            SetDefaultTitle();
        }

        public void ShowBackButton() 
        {
            BackButton.IsVisible = true;
        }

        public void HideBackButton() 
        {
            BackButton.IsVisible = false;
        }

        public void SetDefaultTitle() 
        {
            SetTitle("Home");
        }

        public void SetTitle(string title)
        {
            var title_ = "";

            if (title.Length > 0)
            {
                title_ = title;
            }

            TitleLabel.Text = title_;
        }

        void Handle_Clicked_BackButton(object sender, System.EventArgs e)
        {
            CurrentPage.CloseActiveView();
        }

        void Handle_Clicked_AddButton(object sender, System.EventArgs e)
        {
            CurrentPage.CardForm.Open();
        }

    }
}
