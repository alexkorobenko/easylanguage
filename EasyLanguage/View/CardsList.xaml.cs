﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using EasyLanguage.Model;
using Xamarin.Forms;

namespace EasyLanguage.View
{
    public partial class CardsList : ContentView
    {

        public MainPage CurrentPage;

        public CardsList()
        {
            InitializeComponent();
        }

        public void Render() 
        {
            PageList.ItemsSource = null;
            PageList.ItemsSource = CurrentPage.CardsCollection.List;
        }

        public void Open()
        {
            this.IsVisible = true;

            CurrentPage.Header.SetTitle("Cards List");
            CurrentPage.Header.ShowBackButton();

            CurrentPage.ActiveViewName = "CardsList";

            Render();
        }

        public void Close()
        {
            this.IsVisible = false;

            CurrentPage.Header.SetDefaultTitle();
            CurrentPage.Header.HideBackButton();

            CurrentPage.ActiveViewName = "MainPage";
        }

        void Handle_Clicked_EditButton(object sender, System.EventArgs e)
        {
            var id = (int)((Button)sender).BindingContext;

            CurrentPage.CardForm.Card = CurrentPage.CardsCollection.FindItem(id);
            CurrentPage.CardForm.Open();
        }

        async void Handle_Clicked_RemoveButton(object sender, System.EventArgs e)
        {
            var action = await App.Current.MainPage.DisplayAlert(null, "Remove item ?", "Yes", "No");

            if (action == true)
            {
                var id = (int)((Button)sender).BindingContext;
                CurrentPage.CardsCollection.RemoveItem(id);
                CurrentPage.Update();
            }
        }

    }
}
