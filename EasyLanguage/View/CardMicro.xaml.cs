﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using EasyLanguage.Model;
using Xamarin.Forms;

namespace EasyLanguage.View
{
    public partial class CardMicro : ContentView
    {

        public MainPage CurrentPage;

        public CardModel item;

        private bool flip;

        public CardMicro(CardModel _item)
        {
            item = _item;

            flip = false;

            InitializeComponent();

            Update();
        }

        public void Update()
        {
            BindingContext = null;
            BindingContext = item;
        }

        private void OnTapGestureRecognizerTapped(object sender, EventArgs args)
        {
            if (flip)
            {
                MicroFront.IsVisible = true;
                MicroBack.IsVisible = false;
                flip = false;
            }
            else
            {
                MicroFront.IsVisible = false;
                MicroBack.IsVisible = true;
                flip = true;
            }
        }

    }

}
