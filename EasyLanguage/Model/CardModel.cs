﻿using System;
using System.Diagnostics;

namespace EasyLanguage.Model
{
    public class CardModel
    {

        public int Id { get; set; }
        public string OriginTitle { get; set; }
        public string TranslateTitle { get; set; }
        public string Description { get; set; }

        public CardModel()
        {
            Id = (int)System.DateTime.Now.TimeOfDay.TotalMilliseconds;
        }

    }
}
