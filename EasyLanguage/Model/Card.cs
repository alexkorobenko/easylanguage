﻿using System;
namespace EasyLanguage.Model
{
    public class Card
    {
        public int id;
        public string origin_title;
        public string translate_title;
        public string description;
    }
}